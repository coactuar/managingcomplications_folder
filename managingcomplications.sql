-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 11:03 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `managingcomplications`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Akshat Jharia', 'akshatjharia@gmail.com', 'PUNE', 'COACT', NULL, NULL, '2021/05/04 17:22:48', '2021-05-04', '2021-05-04', 1, 'Abbott'),
(2, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021/05/07 14:04:35', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(3, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021/05/07 17:59:39', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(4, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/07 18:01:02', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(5, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/07 18:05:18', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(6, 'Dr N S Rajendra', 'drraj.rajendra@gmail.com', 'Mysuru', 'NH', NULL, NULL, '2021/05/07 18:16:42', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(7, 'Arun James', 'arun.nelluvelil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/07 18:20:28', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(8, 'pradeep', 'pradeepkd74@yahoo.co.in', 'bengaluru', 'aster', NULL, NULL, '2021/05/07 18:44:55', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(9, 'Rajagopal Jambunathan', 'rajagopalj123@aol.com', 'MYSORE', 'Cauvery Heart Hospital', NULL, NULL, '2021/05/07 18:46:39', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(10, 'Dr Sunil Kumar S', 'sunilbmc98@gmail.com', 'Bangalore', 'Columbia Asia Hospital', NULL, NULL, '2021/05/07 18:47:36', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(11, 'DR SURESH PATTED', 'drpatted@yahoo.COM', 'BELGAUM', 'KLES HOSPITAL', NULL, NULL, '2021/05/07 18:47:41', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(12, 'Rajagopal Jambunathan', 'rajagopalj123@aol.com', 'MYSORE', 'Cauvery Heart Hospital', NULL, NULL, '2021/05/07 18:51:05', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(13, 'Dr Raja Nag', 'dr.raja.nag@gmail.com', 'Kolkata', 'Apollo Gleneagles hospitals', NULL, NULL, '2021/05/07 18:58:11', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(14, 'NSR', 'drraj.rajendra@gmail.com', 'Mysuru', 'NH', NULL, NULL, '2021/05/07 19:00:40', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(15, 'Gopi Aniyathodiyil', 'gopi.a.dr@gmail.com', 'bangalore', 'Fortis hospital', NULL, NULL, '2021/05/07 19:01:37', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(16, 'Dr Raja Nag', 'dr.raja.nag@gmail.com', 'Kolkata', 'Apollo Gleneagles hospitals', NULL, NULL, '2021/05/07 19:09:27', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(17, 'SREEVATSA NADIG', 'nadig33@gmail.com', 'shimoga', 'Sahyadri Narayana Multispeciality Hospital', NULL, NULL, '2021/05/07 19:21:32', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(18, 'Dr Rajendra N S', 'drraj.rajendra@gmail.com', 'Mysuru', 'NH', NULL, NULL, '2021/05/07 19:33:28', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(19, 'Rajagopal ', 'rajagopalj@gmail.com', 'Mysore', 'Cauvery ', NULL, NULL, '2021/05/07 19:57:02', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(20, 'sreevatsa n s', 'nadig33@gmail.com', 'shimoga', 'Sahyadri Narayana Multispeciality Hospital', NULL, NULL, '2021/05/07 20:04:17', '2021-05-07', '2021-05-07', 1, 'Abbott'),
(21, 'Jagadeesh', 'jagadeesh.kalathil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/07 20:25:27', '2021-05-07', '2021-05-07', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Hi\r\n', '2021-05-05 15:44:13', 'Abbott', 0, 0),
(2, 'Dr AJ Swamy', 'ajayswamy@rediffmail.com', 'Pl make it full screen\r\n', '2021-05-07 20:33:38', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-05-04 17:15:29', '2021-05-04 17:15:29', '2021-05-04 18:45:29', 0, 'Abbott', '266570ab273d4b01741d9f855114a02b'),
(2, 'Akshat Jharia', 'akshatjharia@gmail.com', 'PUNE', 'COACT', NULL, NULL, '2021-05-04 17:18:33', '2021-05-04 17:18:33', '2021-05-04 18:48:33', 0, 'Abbott', 'efd4edb95a5b5055abf4ab1fb43ff62e'),
(3, 'Akshat Jharia', 'akshatjharia@gmail.com', 'PUNE', 'COACT', NULL, NULL, '2021-05-05 15:43:22', '2021-05-05 15:43:22', '2021-05-05 17:13:22', 0, 'Abbott', '2e5732db3cfd3bdfccc1048fc254e8ba'),
(4, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-05-05 16:40:01', '2021-05-05 16:40:01', '2021-05-05 18:10:01', 0, 'Abbott', 'b2d11d2a24611670c01e2c09d9ffb9a5'),
(5, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-05-05 17:01:32', '2021-05-05 17:01:32', '2021-05-05 18:31:32', 0, 'Abbott', '02382ca945e8cf6763fbe09587b47b3f'),
(6, 'Praveen kumar', 'jipmer.praveen@gmail.com', 'kolar', 'Fortis ', NULL, NULL, '2021-05-07 08:20:46', '2021-05-07 08:20:46', '2021-05-07 09:50:46', 0, 'Abbott', 'f41d7d68daf4982ffef9f004856608eb'),
(7, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-05-07 14:03:06', '2021-05-07 14:03:06', '2021-05-07 15:33:06', 0, 'Abbott', '71ee327e3b872b1db0576697583e6b64'),
(8, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-07 14:05:25', '2021-05-07 14:05:25', '2021-05-07 15:35:25', 0, 'Abbott', 'ed9fac103a5cc0d59ec194b2e21f31db'),
(9, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-05-07 14:05:37', '2021-05-07 14:05:37', '2021-05-07 15:35:37', 0, 'Abbott', '970e8e3269b08180dc605134f087352d'),
(10, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-07 14:12:37', '2021-05-07 14:12:37', '2021-05-07 14:12:53', 0, 'Abbott', 'b7eca14bed80d89e9c591b33a7053fd9'),
(11, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-07 14:26:58', '2021-05-07 14:26:58', '2021-05-07 15:56:58', 0, 'Abbott', '26b2d80e93803d6b1bbab8191c27e8da'),
(12, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-05-07 16:05:27', '2021-05-07 16:05:27', '2021-05-07 17:35:27', 0, 'Abbott', 'df708545391732998de0c8f7043650c7'),
(13, 'Akshat Jharia', 'akshatjharia@gmail.com', 'PUNE', 'COACT', NULL, NULL, '2021-05-07 16:06:23', '2021-05-07 16:06:23', '2021-05-07 17:36:23', 0, 'Abbott', '3447a8cd523a4e36db125ae7c164ca8a'),
(14, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-07 16:50:27', '2021-05-07 16:50:27', '2021-05-07 16:53:33', 0, 'Abbott', 'a6527fdbc87574b618f1a141405d8cc7'),
(15, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-07 17:21:22', '2021-05-07 17:21:22', '2021-05-07 17:22:01', 0, 'Abbott', 'e81ad60d941a3a7dc62b08a4e2223b93'),
(16, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-07 17:39:50', '2021-05-07 17:39:50', '2021-05-07 17:40:03', 0, 'Abbott', '9fdaf1cf5320ef1828c3fcda45ba58d9'),
(17, 'Dr. Uttam Kumar Sharma', 'uttamsarma@gmail.com', 'Guwahati', 'Nemcare', NULL, NULL, '2021-05-07 18:01:27', '2021-05-07 18:01:27', '2021-05-07 19:31:27', 0, 'Abbott', 'd42bdd1609a8489bd00ff348f04a1d07'),
(18, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-07 18:04:00', '2021-05-07 18:04:00', '2021-05-07 19:35:28', 0, 'Abbott', 'b06713c62dbf41d89c71e8cf576973a0'),
(19, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-05-07 18:27:07', '2021-05-07 18:27:07', '2021-05-07 19:57:07', 0, 'Abbott', '34cd6783dc6a1c6f5578b53de8983d5e'),
(20, 'Kethan Galla', 'gallakethan@gmail.com', 'Bangalore', 'Apollo hospital', NULL, NULL, '2021-05-07 18:43:14', '2021-05-07 18:43:14', '2021-05-07 20:13:14', 0, 'Abbott', 'fb005d73319f739577c929d3ee193800'),
(21, 'Dr Shankaragouda B H', 'sbh0012@yahoo.co.in', 'Kalaburagi', 'SJICR Kalaburagi', NULL, NULL, '2021-05-07 18:46:11', '2021-05-07 18:46:11', '2021-05-07 20:16:11', 0, 'Abbott', '3b7721a2536f9a5f86593e21b08e28aa'),
(22, 'Mahendra Kumar G', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-05-07 18:54:38', '2021-05-07 18:54:38', '2021-05-07 20:24:38', 0, 'Abbott', '26917b624324c0215b637a4f541ed789'),
(23, 'KRISHNA SARIN MS', 'krishna588@gmail.com', 'Bengaluru ', 'Specialist Hospital, Kalyan Nagar', NULL, NULL, '2021-05-07 18:57:05', '2021-05-07 18:57:05', '2021-05-07 20:27:05', 0, 'Abbott', 'db4d81ccf92df8cff7822ccfe0c18595'),
(24, 'Abhilash ', 'abhilash.mohanan@abbott.com', 'Bangalore ', 'Abbott', NULL, NULL, '2021-05-07 18:57:14', '2021-05-07 18:57:14', '2021-05-07 20:27:14', 0, 'Abbott', 'd8044adabc98bd93c1e13baacf6754fb'),
(25, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-07 18:57:42', '2021-05-07 18:57:42', '2021-05-07 20:27:42', 0, 'Abbott', 'cf3210293800ac7bcc70a839ed4eed3c'),
(26, 'Gopi A', 'gopi.a.dr@gmail.com', 'Bengaluru', 'Fortis hospital', NULL, NULL, '2021-05-07 18:58:34', '2021-05-07 18:58:34', '2021-05-07 20:28:34', 0, 'Abbott', '357fb68091a4b5b3c014e149fcd16ff9'),
(27, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-05-07 19:00:06', '2021-05-07 19:00:06', '2021-05-07 20:30:06', 0, 'Abbott', 'f3a17a18a77634a149fd310313394e9c'),
(28, 'Jagadeesh', 'jagadeesh.kalathil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-07 19:00:35', '2021-05-07 19:00:35', '2021-05-07 20:30:35', 0, 'Abbott', '01c8a8352f1a1777ac4f14dded093cc1'),
(29, 'Subramanyam K', 'subramanyam70@yahoo.com', 'Bengaluru ', 'Sjic&r', NULL, NULL, '2021-05-07 19:00:45', '2021-05-07 19:00:45', '2021-05-07 20:30:45', 0, 'Abbott', 'ec4f032f521bc1ad1309604745390641'),
(30, 'sridhar', 'drnsridhara@gmail.com', 'BENGALURU', 'fortis', NULL, NULL, '2021-05-07 19:02:13', '2021-05-07 19:02:13', '2021-05-07 20:32:13', 0, 'Abbott', '12a03d9b455f4463499b87f2c06db6e2'),
(31, 'DR SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-05-07 19:02:31', '2021-05-07 19:02:31', '2021-05-07 20:32:31', 0, 'Abbott', '06f72cccba5eb5ad81f3adceab6883f6'),
(32, 'Pavan Kumar m n ', 'pavanmn9@gmail.com', 'Mysore ', 'Cauvery heart and multispeciality hospital ', NULL, NULL, '2021-05-07 19:02:35', '2021-05-07 19:02:35', '2021-05-07 20:32:35', 0, 'Abbott', 'ff15eb8ac4de232cffb58c21a3e682e2'),
(33, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore ', 'Abbott', NULL, NULL, '2021-05-07 19:03:02', '2021-05-07 19:03:02', '2021-05-07 20:33:02', 0, 'Abbott', 'a61f93d4aa454c4dd6faea1eeed361d8'),
(34, 'Vikranth Veeranna', 'vikranthdm@gmail.com', 'Bengaluru ', 'SSNMC ', NULL, NULL, '2021-05-07 19:03:19', '2021-05-07 19:03:19', '2021-05-07 20:33:19', 0, 'Abbott', '07104e08930670cb636612258aa3b6f2'),
(35, 'l', 'lokeshataipg@rediffmail.com', 'b', 'sjic', NULL, NULL, '2021-05-07 19:03:35', '2021-05-07 19:03:35', '2021-05-07 20:33:35', 0, 'Abbott', '99ac4e8526231553bfb132a8cf9a33ef'),
(36, 'Sharath', 'drsharathps86@gmail.com', 'Shimoga', 'Snmh shimoga', NULL, NULL, '2021-05-07 19:03:50', '2021-05-07 19:03:50', '2021-05-07 20:33:50', 0, 'Abbott', 'e74f5bea4659241eb0ad8c793c02f013'),
(37, 'Muhammed Nizar', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-05-07 19:04:32', '2021-05-07 19:04:32', '2021-05-07 20:34:32', 0, 'Abbott', 'c77fe041168282845d91bbb3980ed29c'),
(38, 'Halesh', 'haleshnaiks@gmail.com', 'Shimoga', 'Narayana Hrudayalaya', NULL, NULL, '2021-05-07 19:04:35', '2021-05-07 19:04:35', '2021-05-07 19:05:21', 0, 'Abbott', '29b588b0caa65a603bf67a1c253112af'),
(39, 'sunil dwivedi', 'anushkaeshaan@gmail.com', 'Bangalore', 'vikram hospital', NULL, NULL, '2021-05-07 19:04:37', '2021-05-07 19:04:37', '2021-05-07 20:34:37', 0, 'Abbott', 'd6e92a6fd26665c1c06caf764b700ee3'),
(40, 'Chirag D', 'chiragnichi@gmail.com', 'Bangalore ', 'Fortis', NULL, NULL, '2021-05-07 19:05:04', '2021-05-07 19:05:04', '2021-05-07 20:35:04', 0, 'Abbott', '646a8fef1012fd5770c6f241e5518045'),
(41, 'Harish', 'harishp2331988@gmail.com', 'Mysuru ', 'Cauvery hospital ', NULL, NULL, '2021-05-07 19:05:25', '2021-05-07 19:05:25', '2021-05-07 20:35:25', 0, 'Abbott', 'edadacd6b67f31bc632c6ffc8d7e8818'),
(42, 'Vindya k p', 'vindyakpc@gmail.com', 'Mysore', 'Cauvery hospital', NULL, NULL, '2021-05-07 19:05:26', '2021-05-07 19:05:26', '2021-05-07 19:07:07', 0, 'Abbott', 'aa3ae972a2c3807a2ac3aadfd138c06f'),
(43, 'Sreedhara R', 'kummas82@gmail.com', 'Mysore ', 'SJICR ', NULL, NULL, '2021-05-07 19:05:46', '2021-05-07 19:05:46', '2021-05-07 20:35:46', 0, 'Abbott', 'e563605a94ff53535bf229de36ba15ee'),
(44, 'Naga Srinivaas', 'drakondi.ns@gmail.com', 'Bengaluru ', 'Manipal ', NULL, NULL, '2021-05-07 19:05:48', '2021-05-07 19:05:48', '2021-05-07 20:35:48', 0, 'Abbott', 'ab0777fdc18fac6babf767c1874a4a31'),
(45, 'Prabhavathi ', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-07 19:09:33', '2021-05-07 19:09:33', '2021-05-07 20:39:33', 0, 'Abbott', '1576d03232e60a0c63354ef6815ad19b'),
(46, 'Dharma teja', 'dharma615@gmail.com', 'Belgaum ', 'Kle', NULL, NULL, '2021-05-07 19:10:23', '2021-05-07 19:10:23', '2021-05-07 20:40:23', 0, 'Abbott', '41be0c91fc32056463b255cd514ab27a'),
(47, 'GKeshavamurthy', 'keshavamurthyg@gmail.com', 'New Delhi', 'AHRR', NULL, NULL, '2021-05-07 19:11:16', '2021-05-07 19:11:16', '2021-05-07 19:18:39', 0, 'Abbott', '9f60ad164f49bdb1b9d11960a800735a'),
(48, 'Atanu Banerjee ', 'atanu.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-05-07 19:11:29', '2021-05-07 19:11:29', '2021-05-07 20:41:29', 0, 'Abbott', '3e059da9ecabd511a8a0cb1dcdbceba9'),
(49, 'Sagar ', 'desaisagar13@gmail.com', 'Bagalkot ', 'Hsk', NULL, NULL, '2021-05-07 19:12:32', '2021-05-07 19:12:32', '2021-05-07 20:42:32', 0, 'Abbott', '6807d0bf704a80924d7350059d696eae'),
(50, 'Vikranth Veeranna', 'vikranthdm@gmail.com', 'Bengaluru ', 'SSNMC ', NULL, NULL, '2021-05-07 19:12:35', '2021-05-07 19:12:35', '2021-05-07 20:42:35', 0, 'Abbott', '5ecbcf58d7e93e547035086f1b673a9f'),
(51, 'Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-05-07 19:13:02', '2021-05-07 19:13:02', '2021-05-07 20:43:02', 0, 'Abbott', 'e6af9e4d9011b370a4d614e7c6e4a8cb'),
(52, 'Dr Aritra Konar ', 'konar1978.dr@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-05-07 19:15:10', '2021-05-07 19:15:10', '2021-05-07 20:45:10', 0, 'Abbott', '9bd91b6595d03c3479e1da86002ee4a9'),
(53, 'Dr Srinidhi Hegde ', 'dr.srihegde@gmail.com', 'Mysuru', 'SJICR', NULL, NULL, '2021-05-07 19:15:22', '2021-05-07 19:15:22', '2021-05-07 20:45:22', 0, 'Abbott', '0a87f9803b60a407fcf3feee77602d64'),
(54, 'Beeresha ', 'beereshp@yahoo.co.in', 'Bengaluru ', 'Sjc ', NULL, NULL, '2021-05-07 19:15:55', '2021-05-07 19:15:55', '2021-05-07 20:45:55', 0, 'Abbott', '0af7534f496b61ea275027908495a4d5'),
(55, 'SACHIT ROY', 'sachit.roy@abbott.com', 'Patna', 'AV', NULL, NULL, '2021-05-07 19:16:03', '2021-05-07 19:16:03', '2021-05-07 20:46:03', 0, 'Abbott', 'e09b8e03fa9ae19b6bb716203b9a6490'),
(56, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-05-07 19:16:18', '2021-05-07 19:16:18', '2021-05-07 20:46:18', 0, 'Abbott', '5f5cc78e09f400127c44391704197b10'),
(57, 'Dr Arijit Ghosh ', 'arijitjoy@gmail.com', 'Kolkata', 'AMRI Salt Lake ', NULL, NULL, '2021-05-07 19:18:07', '2021-05-07 19:18:07', '2021-05-07 20:48:07', 0, 'Abbott', 'c5c9d782c4e93b5929c3315c8238aea9'),
(58, 'G Keshavamurthy', 'keshavamurthyg@gmail.com', 'New Delhi', 'AHRR', NULL, NULL, '2021-05-07 19:18:17', '2021-05-07 19:18:17', '2021-05-07 20:48:17', 0, 'Abbott', '32e866cf245d7305a326d9cfe78971cc'),
(59, 'Tejeshwar reddy', 'rede237@gmail.com', 'Bangalore', 'Jayadeva hospital', NULL, NULL, '2021-05-07 19:20:03', '2021-05-07 19:20:03', '2021-05-07 20:50:03', 0, 'Abbott', '96ea822f1ce8e838953929f6e5caf24c'),
(60, 'Naga Srinivaas', 'drakondi.ns@gmail.com', 'Bengaluru ', 'Manipal ', NULL, NULL, '2021-05-07 19:22:13', '2021-05-07 19:22:13', '2021-05-07 20:52:13', 0, 'Abbott', '9bc468b1b6f4c50ffb11b0e9945f6794'),
(61, 'l', 'lokeshataipg@rediffmail.com', 'b', 'sjic', NULL, NULL, '2021-05-07 19:22:34', '2021-05-07 19:22:34', '2021-05-07 20:52:34', 0, 'Abbott', 'd125abfb6f1c43cca184696abb0be41b'),
(62, 'Dr. Uttam Kumar Sharma', 'uttamsarma@gmail.com', 'Guwahati', 'Nemcare', NULL, NULL, '2021-05-07 19:24:45', '2021-05-07 19:24:45', '2021-05-07 20:54:45', 0, 'Abbott', '9561bccd16c15f41274ec9eed238b949'),
(63, 'Prabhavathi ', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-07 19:25:54', '2021-05-07 19:25:54', '2021-05-07 20:55:54', 0, 'Abbott', '1575b21ee36581b182c4252d6fddd6c3'),
(64, 'Prabhavathi ', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-07 19:26:52', '2021-05-07 19:26:52', '2021-05-07 20:56:52', 0, 'Abbott', '8ee5e43e9fb3fa96092bb7bfc6ef9eb7'),
(65, 'Pavan Kumar m n ', 'pavanmn9@gmail.com', 'Mysore ', 'Cauvery heart and multispeciality hospital ', NULL, NULL, '2021-05-07 19:27:06', '2021-05-07 19:27:06', '2021-05-07 20:57:06', 0, 'Abbott', 'a409d6f043dfbfba3bb62876b5c22e43'),
(66, 'Harish', 'harishp2331988@gmail.com', 'Mysuru ', 'Cauvery hospital ', NULL, NULL, '2021-05-07 19:27:09', '2021-05-07 19:27:09', '2021-05-07 19:27:44', 0, 'Abbott', 'b123d79cdd71fef9cc20e42b53283f06'),
(67, 'Dr Ashalatha', 'ashalathab48@gmail.com', 'Bangalore', 'Saptagiri institute of medical sciences and research', NULL, NULL, '2021-05-07 19:27:51', '2021-05-07 19:27:51', '2021-05-07 19:35:27', 0, 'Abbott', '4e86c39928e031f4167973bc4a268037'),
(68, 'Neelesh Kumar S Shah', 'neeleshshah110@gmail.com', 'Belgaum', 'KLE\'s Dr Prabhakar Kore hospital and research centre', NULL, NULL, '2021-05-07 19:28:05', '2021-05-07 19:28:05', '2021-05-07 20:58:05', 0, 'Abbott', '43d0c88951469a41269c6010934b5665'),
(69, 'Prabhavathi Enter Last Name', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-07 19:28:08', '2021-05-07 19:28:08', '2021-05-07 20:58:08', 0, 'Abbott', '932fe71ccc5183078eecdaab7d4f1f93'),
(70, 'Neelesh Kumar S Shah', 'neeleshshah110@gmail.com', 'Belgaum', 'KLE\'s Dr Prabhakar Kore hospital and research centre', NULL, NULL, '2021-05-07 19:28:26', '2021-05-07 19:28:26', '2021-05-07 20:58:26', 0, 'Abbott', '2ea63bfb9fc608f3aef77aa98f32317f'),
(71, 'Sudeep', 'sudeepachar.124@gmail.com', 'Bangalore', 'Apollo', NULL, NULL, '2021-05-07 19:32:09', '2021-05-07 19:32:09', '2021-05-07 21:02:09', 0, 'Abbott', '62edb5f4c24be4a4a477048bdbc92150'),
(72, 'Vikranth Veeranna', 'vikranthdm@gmail.com', 'Bengaluru ', 'SSNMC ', NULL, NULL, '2021-05-07 19:34:25', '2021-05-07 19:34:25', '2021-05-07 21:04:25', 0, 'Abbott', '9b75f69a36836c7d8a83c032c7f85725'),
(73, 'Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-05-07 19:35:16', '2021-05-07 19:35:16', '2021-05-07 21:05:16', 0, 'Abbott', '9ad3f4dcdf15c83c2362fdcbf6bca2d4'),
(74, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-07 19:35:34', '2021-05-07 19:35:34', '2021-05-07 20:57:43', 0, 'Abbott', '662da9efe54d9b3a9c59d3e172ba2fee'),
(75, 'Dr Ashalatha', 'ashalathab48@gmail.com', 'Bangalore', 'Saptagiri institute of medical sciences and research', NULL, NULL, '2021-05-07 19:35:47', '2021-05-07 19:35:47', '2021-05-07 21:05:47', 0, 'Abbott', '0f069212bc77488cd1a860ce58dca23a'),
(76, 'Dharma ', 'dharma615@gmail.com', 'Belgaum ', 'Kle', NULL, NULL, '2021-05-07 19:36:05', '2021-05-07 19:36:05', '2021-05-07 21:06:05', 0, 'Abbott', '94396ac0c2c616213d3960bd358b6c29'),
(77, 'Dr yathish', 'yathishklr78@gmail.com', 'Mysore', 'Cauvery heart and multi speciality hospital', NULL, NULL, '2021-05-07 19:40:08', '2021-05-07 19:40:08', '2021-05-07 21:10:08', 0, 'Abbott', 'a0e087dff380b36b6dcc58d592e42295'),
(78, 'Pavan Kumar m n ', 'pavanmn9@gmail.com', 'Mysore ', 'Cauvery heart and multispeciality hospital ', NULL, NULL, '2021-05-07 19:40:14', '2021-05-07 19:40:14', '2021-05-07 21:10:14', 0, 'Abbott', '0ff5128dd0b31905be95d9c68bbc585b'),
(79, 'Keshava ', 'drkeshavar@gmail.com', 'Bangalore ', 'Fortis ', NULL, NULL, '2021-05-07 19:43:03', '2021-05-07 19:43:03', '2021-05-07 21:13:03', 0, 'Abbott', '702680a06bb3c7554dc8f5c38cb71659'),
(80, 'Tejeshwar reddy', 'rede237@gmail.com', 'Bangalore', 'Jayadeva hospital', NULL, NULL, '2021-05-07 19:45:03', '2021-05-07 19:45:03', '2021-05-07 21:15:03', 0, 'Abbott', '44c37386a5692b6f3f7668e5f8cd43c4'),
(81, 'Pavan Kumar m n ', 'pavanmn9@gmail.com', 'Mysore ', 'Cauvery heart and multispeciality hospital ', NULL, NULL, '2021-05-07 19:47:05', '2021-05-07 19:47:05', '2021-05-07 21:17:05', 0, 'Abbott', '2f5258a858f591fe0339899b1be35cd3'),
(82, 'Srinivas P', 'drpsrinivas@gmail.com', 'Mysore ', 'NH', NULL, NULL, '2021-05-07 19:47:46', '2021-05-07 19:47:46', '2021-05-07 21:17:46', 0, 'Abbott', '5bd28876cf6f1cbb66a662eac348216f'),
(83, 'Disha Shetty', 'dishaganeshshetty@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-05-07 19:48:19', '2021-05-07 19:48:19', '2021-05-07 21:18:19', 0, 'Abbott', '32a1fd0a2dac4f416fd698866a3871fd'),
(84, 'Sharad Masudi', 'sharad.masudi@gmail.com', 'Bangalore', 'Sri Jayadeva institute of Cardiology', NULL, NULL, '2021-05-07 19:52:38', '2021-05-07 19:52:38', '2021-05-07 21:22:38', 0, 'Abbott', 'ac96e83ccc0900eb34cd109ddb57a485'),
(85, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-07 19:53:57', '2021-05-07 19:53:57', '2021-05-07 21:23:57', 0, 'Abbott', 'fc002a2555e96b472fe4dfa7597f0fdd'),
(86, 'Pavan Kumar m n ', 'pavanmn9@gmail.com', 'Mysore ', 'Cauvery heart and multispeciality hospital', NULL, NULL, '2021-05-07 19:58:08', '2021-05-07 19:58:08', '2021-05-07 21:28:08', 0, 'Abbott', '3a3c9aff678999a37ba88c990186676b'),
(87, 'Pavan Kumar m n ', 'pavanmn9@gmail.com', 'Mysore ', 'Cauvery heart and multispeciality hospital', NULL, NULL, '2021-05-07 19:59:50', '2021-05-07 19:59:50', '2021-05-07 21:29:50', 0, 'Abbott', '2c2104eea0b83351e3d9647bca8a334f'),
(88, 'Pavan Kumar m n ', 'pavanmn9@gmail.com', 'Mysore ', 'Cauvery heart and multispeciality hospital', NULL, NULL, '2021-05-07 20:00:12', '2021-05-07 20:00:12', '2021-05-07 21:30:12', 0, 'Abbott', '4bba2b4a47379464cd21ac2cd0e6ee59'),
(89, 'Praveen kumar', 'jipmer.praveen@gmail.com', 'kolar', 'G', NULL, NULL, '2021-05-07 20:10:01', '2021-05-07 20:10:01', '2021-05-07 21:40:01', 0, 'Abbott', 'cd9ff9f8e2582338c94bd3e6905af4f0'),
(90, 'DR SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-05-07 20:12:04', '2021-05-07 20:12:04', '2021-05-07 21:42:04', 0, 'Abbott', '59bae15ecfc151e9d8bff43f05485eb2'),
(91, 'sagar desai', 'desaisagar13@gmail.com', 'Bagalkot ', 'Hsk', NULL, NULL, '2021-05-07 20:12:51', '2021-05-07 20:12:51', '2021-05-07 21:42:51', 0, 'Abbott', 'a5dac24a215cd9b36b5a3d790785715a'),
(92, 'Ravikanth Ramadhenu', 'ramadhenravi@gmail.com', 'Palakol', 'Jnmc', NULL, NULL, '2021-05-07 20:14:20', '2021-05-07 20:14:20', '2021-05-07 21:44:20', 0, 'Abbott', '75af492f0453bc32bba58138125c1949'),
(93, 'Vikranth Veeranna', 'vikranthdm@gmail.com', 'Bengaluru ', 'SSNMC ', NULL, NULL, '2021-05-07 20:16:36', '2021-05-07 20:16:36', '2021-05-07 21:46:36', 0, 'Abbott', '3c3489d2d1d2297c5c4801726987ac79'),
(94, 'Pooja', 'pooja@coact.co.in', 'Mumbai', 'Test', NULL, NULL, '2021-05-07 20:18:10', '2021-05-07 20:18:10', '2021-05-07 21:48:10', 0, 'Abbott', '786cdf4ee704a39fc87cd239160799de'),
(95, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore ', 'Abbott', NULL, NULL, '2021-05-07 20:20:01', '2021-05-07 20:20:01', '2021-05-07 21:50:01', 0, 'Abbott', 'e1226ad3845504346f3d022d917b5195'),
(96, 'Keshavamurthy CB', 'cbkeshavamurthy@rediffmail.com', 'Mysore', 'Columbia Asia hospital', NULL, NULL, '2021-05-07 20:22:42', '2021-05-07 20:22:42', '2021-05-07 20:56:34', 0, 'Abbott', '7d53f0aa96d6c9ef45071bfe208fee5b'),
(97, 'Pavan Kumar m n ', 'pavanmn9@gmail.com', 'Mysore ', 'Cauvery heart and multispeciality hospital ', NULL, NULL, '2021-05-07 20:23:36', '2021-05-07 20:23:36', '2021-05-07 21:53:36', 0, 'Abbott', 'ad003c041c17e90ea6e032bf456d8946'),
(98, 'Dr AJ Swamy', 'ajayswamy@rediffmail.com', 'Bangalore', 'Command hospital', NULL, NULL, '2021-05-07 20:23:49', '2021-05-07 20:23:49', '2021-05-07 21:53:49', 0, 'Abbott', '5d34b6e2979608026a2862a3c2937851'),
(99, 'Pavan Kumar m n ', 'pavanmn9@gmail.com', 'Mysore ', 'Cauvery heart and multispeciality hospital ', NULL, NULL, '2021-05-07 20:24:49', '2021-05-07 20:24:49', '2021-05-07 21:54:49', 0, 'Abbott', 'b7483e17c18e9454a66adddeab360e71'),
(100, 'Sudhakar Reddy Pathakota', 'sudhakarreddy.pathakota@gmail.com', 'Hyderabad ', 'Care ', NULL, NULL, '2021-05-07 20:29:12', '2021-05-07 20:29:12', '2021-05-07 21:59:12', 0, 'Abbott', '2db0512b768ef190a34e28ce6c83731e'),
(101, 'Disha Shetty', 'dishaganeshshetty@gmail.com', 'Bangalore', 'Sjic', NULL, NULL, '2021-05-07 20:40:06', '2021-05-07 20:40:06', '2021-05-07 20:56:59', 0, 'Abbott', '366a41a9aeb9ca228bbe4a157f35beec'),
(102, 'Srinivas P', 'drpsrinivas@gmail.com', 'Mysore ', 'NH', NULL, NULL, '2021-05-07 20:42:01', '2021-05-07 20:42:01', '2021-05-07 22:12:01', 0, 'Abbott', 'be1a55fc5bab61a8d1d301754980c978'),
(103, 'Dr AJ Swamy', 'ajayswamy@rediffmail.com', 'Bangalore', 'CommAND HOSPITAL', NULL, NULL, '2021-05-07 20:59:39', '2021-05-07 20:59:39', '2021-05-07 22:29:39', 0, 'Abbott', '7990253caa818a08d579a02e805b02a6'),
(104, 'DR AJ SWAMY', 'ajayswamy@rediffmail.com', 'Bangalore', 'Command Hospital', NULL, NULL, '2021-05-07 21:00:38', '2021-05-07 21:00:38', '2021-05-07 22:30:38', 0, 'Abbott', 'b30399f9f455e4e8037055f5893d812f'),
(105, 'Neelesh Kumar S Shah', 'neeleshshah110@gmail.com', 'TUMKUR', 'KLE\'s Dr Prabhakar Kore hospital and research centre', NULL, NULL, '2021-05-07 21:12:11', '2021-05-07 21:12:11', '2021-05-07 22:42:11', 0, 'Abbott', '9b0b24266627b2e7e464552dd15c21ba'),
(106, 'Neelesh Kumar S Shah', 'neeleshshah110@gmail.com', 'TUMKUR', 'KLE\'s Dr Prabhakar Kore hospital and research centre', NULL, NULL, '2021-05-07 21:16:44', '2021-05-07 21:16:44', '2021-05-07 22:46:44', 0, 'Abbott', '153022aa2c1a1828212d09cfd9324af5'),
(107, 'Neelesh Kumar S Shah', 'neeleshshah110@gmail.com', 'TUMKUR', 'KLE\'s Dr Prabhakar Kore hospital and research centre', NULL, NULL, '2021-05-07 21:19:12', '2021-05-07 21:19:12', '2021-05-07 22:49:12', 0, 'Abbott', 'bab6de697f34a119eb4fb0421594c56a'),
(108, 'Tom Devasia', 'tomdevasia@hotmail.com', 'Manipal', 'KH MANIPAL', NULL, NULL, '2021-05-07 21:21:59', '2021-05-07 21:21:59', '2021-05-07 21:22:16', 0, 'Abbott', 'ecc2f688bb510d3b970d3245840ee294'),
(109, 'Veenu john', 'veenu.remaliya@gmail.com', 'MYSURU', 'Apollo hospital ', NULL, NULL, '2021-05-07 21:40:00', '2021-05-07 21:40:00', '2021-05-07 23:10:00', 0, 'Abbott', '4667270dcaa48a79d0482d4670c848c3'),
(110, 'Beeresha ', 'beereshp@yahoo.co.in', 'Bengaluru ', 'SJICR ', NULL, NULL, '2021-05-09 20:09:54', '2021-05-09 20:09:54', '2021-05-09 21:39:54', 0, 'Abbott', 'cbb3951c271f4824a0f7c8e2f00e9b8d'),
(111, 'Mahendra Kumar G', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-05-25 18:37:47', '2021-05-25 18:37:47', '2021-05-25 20:07:47', 1, 'Abbott', 'ca2a610e08ff06d83568d481bfa6062d'),
(112, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-06-16 18:16:13', '2021-06-16 18:16:13', '2021-06-16 19:46:13', 1, 'Abbott', '941071ad0a6d0f541338f9bb5334502e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
